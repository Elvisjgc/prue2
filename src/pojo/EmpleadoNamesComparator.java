package pojo;

import java.util.Comparator;

/**
 *
 * @author Elvis
 */
public class EmpleadoNamesComparator implements Comparator<Empleado>{

    @Override
    public int compare(Empleado o1, Empleado o2) {
        return o1.getNames().compareTo(o2.getNames());
    }
    
}
