
package pojo;

import java.time.LocalDate;


public class Empleado implements Comparable<Empleado> {

    private int id;
    private String names;
    private double Salary;
   // private double Cedula;
    private LocalDate start_date;
    private APELLIDO apellido;

    public Empleado() {
    }

    public Empleado(int id, String names, double Salary, LocalDate start_date, APELLIDO apellido) {
        this.id = id;
        this.names = names;
        this.Salary = Salary;
        this.start_date = start_date;
        this.apellido = apellido;
       // this.Cedula = Cedula;
    }

    public Empleado(int i, String carla, int i0, int i1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Empleado(int i, String carla, int i0, LocalDate of, APELLIDO apellido) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public APELLIDO getApellido() {
        return apellido;
    }

    public void setCargo(APELLIDO apellido) {
        this.apellido = apellido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public double getSalary() {
        return Salary;
    }

    public void setSalary(double Salary) {
        this.Salary = Salary;
    }
    
   // public double getCedula() {
    //    return Cedula;
  //  }
    
  //  public void setCedula(double Cedula){
  //      this.Cedula = Cedula;
  //  }

    public LocalDate getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDate start_date) {
        this.start_date = start_date;
    }

    @Override
    public int compareTo(Empleado o) {
        return this.id == o.getId() ? 0 : (this.id < o.getId()) ? -1 : 1;
    }

 //   public String getApellido() {
 //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 //   }

 }
