
package collection;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import pojo.APELLIDO;
import pojo.Empleado;
import pojo.EmpleadoNamesComparator;

/**
 *
 * @author Elvis
 */
public class ListTest {
    public static void main(String[] args) {
        List<Empleado> empleados = new ArrayList<>();
        empleados.add(new Empleado(1, "Carla", 16000,
                LocalDate.of(2010, Month.APRIL, 20),APELLIDO.ROJAS));
                
        Collections.sort(empleados,new EmpleadoNamesComparator());
        empleados.stream().filter(e -> e.getSalary() > 14000)   
                .map((e) -> {
            System.out.println("Id: " + e.getId());
            return e;
        }).map((e) -> {
            System.out.println("Nombres: " + e.getNames());
            return e;
        }).map((e) -> {
            System.out.println("Salario: " + e.getSalary());
            return e; 
        }).map(e -> {
            System.out.println("Apellido: " + e.getApellido());
            return e;
          //  System.out.println("Cedula: " + e.getCedula());
        //    return e;
        }).forEachOrdered((e) -> {
            System.out.println("Fecha de contratacion: "
                    + e.getStart_date().toString());
        });
    }
}
